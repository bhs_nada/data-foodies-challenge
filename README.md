# Data Foodies Challenge

Welcome to the Data Foodies challenge, a fun and rewarding opportunity to enhance your data engineering skills while indulging in the world of culinary delights!

## Challenge Overview

Imagine yourself as a **data engineer** working at a company known as _Food Magic_. Your mission? To build a robust database containing 200 recipes for a team of data scientists who are passionate about exploring the world of culinary creations. These data scientists don't have specific recipes in mind; they simply crave a rich dataset of diverse recipes. What they do care about is the quality of information associated with each recipe. Specifically, they want to capture the following details:

- `recipe_name`: The name of the recipe.
- `is_vegetarian`: A boolean indicating whether the recipe is vegetarian (true) or not (false).
- `is_vegan`: A boolean indicating whether the recipe is vegan (true) or not (false).
- `is_gluten_free`: A boolean indicating whether the recipe is gluten-free (true) or not (false).
- The list of ingredients.

That's where you come in! Your mission is to construct a robust [PostgreSQL database](https://www.postgresql.org/download/) housing these 200 recipes and their associated details.

## Data Source: Spoonacular API

To accomplish this culinary data engineering feat, you will utilize the [Spoonacular API](https://spoonacular.com/food-api). Specifically, you will tap into the [random recipes endpoint](https://spoonacular.com/food-api/docs#Get-Random-Recipes) of the API:

```
GET https://api.spoonacular.com/recipes/random
```

Before diving into the API, you'll need to create an account (don't worry, it's free!) to obtain an API key. We recommend exploring the data returned by this endpoint to understand where to extract the relevant recipe information. You might find tools like [Postman](https://www.postman.com/downloads/) helpful for this purpose.

## ETL Script

To populate the recipe database you're aiming to create, you'll write a Python script to serve as your Extract, Transform, Load (ETL) tool. This script will fetch data from the Spoonacular API, transform it into a structured format, and load it into your PostgreSQL database.

Get ready for a delicious data engineering journey and happy coding!
